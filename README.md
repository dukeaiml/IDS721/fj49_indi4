# AWS Step Function Project (INDIVIDUAL 4)

## Youtube video
Check out the demo video [here](https://youtu.be/ffU_-REr32I)


This project allows users to add people data to a DynamoDB table (`indi4`) using AWS Lambda functions and a Step Function. It also provides a functionality to count the number of entries in the table and return the count as a JSON payload.

## Introduction

The project utilizes 2 AWS Lambda functions and are wrapped into a Step Function to manage data in a DynamoDB table. It provides a simple and scalable solution for adding, retrieving, and processing data in the table.

## Features

- `func1`:  Add people data to the `indi4` DynamoDB table
- `func2`:  Count the number of entries in the `indi4` table
- Return the count as a JSON payload

## Setup

### Prerequisites

- AWS account with appropriate permissions
- Rust programming language installed
- AWS CLI installed and configured with appropriate credentials

### Installation

1. Clone the repository to your local machine:

```bash
git clone https://github.com/yourusername/fj49_indi4.git
```

2. Navigate to the project directory for each function and then:

```bash
cargo build
```

and then 

```bash
cargo lambda deploy --iam-role arn:aws:iam::your_role:role/your_project_name
```

you can invoke the functions locally with

```bash
aws lambda invoke
```

## Usage

1. Deploy the Lambda functions and Step Function using AWS CLI or AWS Management Console.
2. Configure the necessary environment variables and permissions for the Lambda functions.
3. Test the functionality using the provided payload JSON files or custom input data.
4. Monitor the execution of the Step Function in the AWS Step Functions console.

## Screenshots

### Invoking the first function
![Screenshot 1](screenshots/func1_invoke.png)

### Testing on AWS console
![Screenshot 1](screenshots/input_json_func1.png)

### Function 2 working
![Screenshot 1](screenshots/func2_working.png)

### Step Function working
![Screenshot 1](screenshots/step_func_working.png)

### Output for both 
![Screenshot 1](screenshots/output_both.png)




### 

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Author 

Faraz Jawed