use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Serialize)]
struct Response {
    total_items: usize,
}

async fn function_handler(event: LambdaEvent<Value>) -> Result<Response, Error> {
    // You can access the event payload if needed
    // let payload = event.payload;

    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);
    let resp = client.scan().table_name("indi4").send().await?;
    let total_items = if let Some(items) = resp.items {
        items.len()
    } else {
        0
    };

    let response = Response { total_items };
    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}